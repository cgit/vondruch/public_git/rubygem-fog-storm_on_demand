# Generated from fog-storm_on_demand-0.1.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name fog-storm_on_demand

Name: rubygem-%{gem_name}
Version: 0.1.0
Release: 1%{?dist}
Summary: Module for the 'fog' gem to support StormOnDemand
Group: Development/Languages
License: MIT
URL: https://github.com/fog/fog-storm_on_demand
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildArch: noarch

%description
This library can be used as a module for `fog` or as standalone provider
to use the StormOnDemand in applications.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
gem build %{gem_name}.gemspec

%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


%check
pushd .%{gem_instdir}
# No tests available :/
popd

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.*
%license %{gem_instdir}/LICENSE.md
%exclude %{gem_instdir}/fog-storm_on_demand.gemspec
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CONTRIBUTING.md
%doc %{gem_instdir}/CONTRIBUTORS.md
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%{gem_instdir}/gemfiles
%{gem_instdir}/spec

%changelog
* Wed Mar 11 2015 Vít Ondruch <vondruch@redhat.com> - 0.1.0-1
- Initial package
